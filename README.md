# Example projects for Godot game engine
</br>
Personal projects for godot</br>
</br>
<h3><strong>Submodules</strong></h3>
</br>
More info on submodules: https://git-scm.com/book/en/v2/Git-Tools-Submodules</br>
</br>
The following are personal notes</br>
To clone the project with submodules use the following command</br>
$ git clone --recurse-submodules git@github.com:FelixRiddle/godot-projects.git</br>
</br>
Updating submodules in one command for slackers like me</br>
$ git submodule update --remote --init --recursive --merge</br>
</br>
Working with the project and submodules</br>
To make git check if the submodule is updated every time we push use</br>
$ git config push.recurseSubmodules check</br>
</br>
The "on-demand" will try to push submodules</br>
$ git config push.recurseSubmodules on-demand</br>
</br>
These are some useful aliases</br>
$ git config alias.spush 'push --recurse-submodules=on-demand'</br>
$ git config alias.supdate 'submodule update --remote --init --recursive --merge'</br>
</br>
