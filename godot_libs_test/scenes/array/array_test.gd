tool
extends Node

var ArrayUtils = preload("res://godot-libs/libs/utils/array_utils.gd")
var Slot = preload("res://godot-libs/inventory_ui/cell/cell.tscn")

func _init():
	#assign_error()
	#increase_size()
	#decrease_size()
	#get_last_three()
	#reverse_array()
	#change_array_size()
	#create_with_type()
	smart_type_detection()
	pass


func assign_error():
	var zero_array = []
	zero_array = ArrayUtils.create_zero_array(5)
	print("Array length is 5")
	print("Setting on index 4:")
	zero_array[4] = 10
	print("Setting on index 5:")
	zero_array[5] = 10


func change_array_size():
	var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	print("Array: ", arr)
	
	print("Reducing size by 4")
	var dict = ArrayUtils.change_length(arr, 4)
	print("Old array: ", dict["old_array"])
	print("New array: ", dict["new_array"])
	arr = dict["new_array"]
	print("Deleted items: ", dict["deleted_items"])
	
	print("Increasing the size by 10")
	dict = ArrayUtils.change_length(arr, 10)
	print("Old array: ", dict["old_array"])
	print("New array: ", dict["new_array"])


func create_with_type():
	print(" --- Create array with type --- ")
	var slot = Slot.instance()
	var arr = ArrayUtils.create_array_with(slot, 10)
	print("Created array with Slot scene, array: ", arr)
	for i in range(10):
		var curr = arr[i]
		print("Slot margin bottom: ", curr.margin_bottom)


func decrease_size():
	var arr = ArrayUtils.create_zero_array(5)
	print("Array: ", arr)
	print("Reducing the size by 4")
	arr = ArrayUtils.shrink_array(arr, 4)
	print("New array: ", arr)


func get_last_three():
	var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	print("Array: ", arr)
	print("The last three items: ", ArrayUtils.get_last_items(arr, 3))


func increase_size():
	var arr = ArrayUtils.create_zero_array(5)
	print("Array: ", arr)
	print("Increasing the size by 5")
	arr = ArrayUtils.increase_length(arr, 5)
	print("New array: ", arr)


func print_object_reference_arr_props(arr, prop_name):
	if(!arr || typeof(arr) != TYPE_ARRAY):
		return
	
	for i in range(arr.size()):
		var curr = arr[i]
		if(curr is Object && curr.get(prop_name)):
			print(prop_name, "(", i, "): ", curr[prop_name])


func reverse_array():
	var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	print("Array: ", arr)
	print("Array reversed: ", ArrayUtils.reverse_array(arr))


func smart_type_detection():
	print(" --- Smart type detection ---")
	print("Smart create array with")
	var arr = ArrayUtils.smart_create_array_with(
		"res://godot-libs/inventory_ui/inventory_manager.tscn", 5)
	
	print("Array: ", arr)
	print("Filename(s):")
	print_object_reference_arr_props(arr, "filename")
	
	####
	print("")
	print("Smart increase size")
	arr = ArrayUtils.smart_increase_length(arr, 5, \
			"res://godot-libs/inventory/items/item.gd")
	print("New array: ", arr)
	
	######
	print("")
	print("Smart change size, set to 15(increasing 5)")
	var new_arr = ArrayUtils.smart_change_size(
		arr,
		15,
		"res://godot-libs/inventory/inventory.gd")
	arr = new_arr["new_array"]
	print("New array: ", arr)
	print_object_reference_arr_props(arr, "filename")
	
	print("")
	print("Smart change size, set to 20(increasing 5)")
	new_arr = ArrayUtils.smart_change_size(
		arr,
		20,
		"res://godot-libs/inventory_ui/cell/cell.tscn")
	arr = new_arr["new_array"]
	print("New array: ", arr)
	print_object_reference_arr_props(arr, "filename")
	
	###
	print("")
	print("Smart change size, set to 7(decrese by 13)")
	new_arr = ArrayUtils.smart_change_size(
		arr,
		7,
		"res://godot-libs/inventory_ui/cell/cell.tscn")
	arr = new_arr["new_array"]
	print("New array: ", arr)
	print_object_reference_arr_props(arr, "filename")
