extends Node

var InventoryManagerScene = preload(
		"res://godot-libs/inventory_ui/inventory_manager.tscn")

# Instancing doesn't work very well on tool mode
func _ready():
	create_inventory_manager()
	print_screen_dimensions()
	pass


##
func create_inventory_manager():
	print("--- Inventory manager test --- ")
	print("Instancing inventory manager scene")
	var inv_mng = InventoryManagerScene.instance()
	add_child(inv_mng)
	
	inv_mng.debug = true
	
	# Load inventory interfaces(scenes)
	var InventoryUI = preload("res://godot-libs/inventory_ui/" + \
			"inventory/inventory.tscn")
	var Hotbar = preload("res://godot-libs/inventory_ui/hotbar/hotbar.tscn")
	
	print("Adding 2 hotbars")
	var hotbar_scene1 = inv_mng.add_inventory_scene(Hotbar)
	var hotbar_scene2 = inv_mng.add_inventory_scene(Hotbar)
	print("Adding an inventory")
	var inventory = inv_mng.add_inventory_scene(InventoryUI)
	inventory.get_cells_manager().set_info({
		"cells_min_size": get_viewport().size.x * 0.05,
		"debug": true,
		"length": 5,
	})
	
	# Inventory scenes
	var hotbar1 = hotbar_scene1.get_inventory()
	var hotbar2 = hotbar_scene1.get_inventory()
	# The inventory script
	var inv = inventory.get_inventory()
	print("Inventory/ies added")
	
	print("Debug: ", inv.debug)
	print("Size: ", inv.size)
	print("Items: ", inv.items)
	
	inv.size = 63
	print("Added 5 slots to the inventory")


func print_screen_dimensions():
	print("--- Viewport test ---")
	var viewport_dim = get_viewport().size
	print("Viewport dimensions: ", viewport_dim)
	
	print("10% of viewport width: ", viewport_dim.x * 0.1)
	print("5% of viewport width: ", viewport_dim.x * 0.05)
	print("1% of viewport width: ", viewport_dim.x * 0.01)
