tool
extends Node

var ArrayUtils = preload("res://godot-libs/libs/utils/array_utils.gd")
var inv_ui = preload("res://godot-libs/inventory_ui/"+ \
		"inventory_manager.tscn").instance()

####
func _init():
	reference_test()
	reference_test2()
	pass


func inexistent_prop():
	print("Inexistent prop: ", inv_ui.inexistent_prop)


func print_object_reference_arr_props(arr, prop_name):
	if(!arr || typeof(arr) != TYPE_ARRAY):
		return
	
	for i in range(arr.size()):
		var curr = arr[i]
		print(prop_name, "(", i, "): ", curr[prop_name])


func reference_test():
	print(" --- Reference test --- ")
	
	var Cell = preload("res://godot-libs/inventory_ui/cell/cell.tscn")
	# If every instance references the same item, every instance will have
	# the same value, for example:
	#var result:Dictionary = ArrayUtils.change_size(cells, value, \
	#	Cell.instance()) # <- It's using the same instance for every item
	var arr = []
	print("Array: ", arr)
	
	arr = ArrayUtils.change_length(arr, 10, Cell.instance())["new_array"]
	print("New array: ", arr)
	
	# Print margin bottom property of every item in arr
	print_object_reference_arr_props(arr, "margin_bottom")
	
	# Change margin_bottom value of the fifth(index 4) element
	arr[5]["margin_bottom"] = 100
	print("Margin bottom of the fifth element changed to 100")
	
	# Print margin bottom property of every item in arr
	print_object_reference_arr_props(arr, "margin_bottom")

func reference_test2():
	print(" --- Reference test 2 --- ")
	var arr = ArrayUtils.create_array_with(0, 10)
	print("Array: ", arr)
	
	arr[5] = 1
	print("Changed the fifth element value for 1, array: ", arr)
