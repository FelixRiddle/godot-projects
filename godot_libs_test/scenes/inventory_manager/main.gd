extends Node

var InventoryManagerScene = preload(
		"res://godot-libs/inventory_ui/inventory_manager.tscn")

# Instancing doesn't work very well on tool mode
func _ready():
	#create_inventory_manager()
	#change_cells_texture()
	set_inventory_ui_info()
	pass


func set_inventory_ui_info():
	print("--- Set InventoryUI key values ---")
	var inv_mng:InventoryManager = InventoryManagerScene.instance()
	add_child(inv_mng)
	inv_mng.debug = true
	
	print("Setting inv_mng name to 'NewName'")
	inv_mng.set_info({ "name": "NewName" })
	
	# Load inventory interfaces(scenes)
	var InventoryUI = preload("res://godot-libs/inventory_ui/" + \
			"inventory/inventory.tscn")
	
	print("Adding a inventory to InventoryManager")
	var inventory_ui:InventoryUI = inv_mng.add_inventory_scene(InventoryUI)
	inventory_ui.debug = true
	
	print("Changing InventoryUI visible and name properties")
	inventory_ui.set_info({ "name": "NewInventoryUIName", "visible": false })


func change_cells_texture():
	print("--- Change cells textures ---")
	var inv_mng = InventoryManagerScene.instance()
	add_child(inv_mng)
	inv_mng.debug = true
	
	# Load inventory interfaces(scenes)
	var InventoryUI = preload("res://godot-libs/inventory_ui/" + \
			"inventory/inventory.tscn")
	
	print("Adding a inventory to InventoryManager")
	var inventory_ui = inv_mng.add_inventory_scene(InventoryUI)
	inventory_ui.get_cells_manager().set_info({
		"cells_min_size": get_viewport().size.x * 0.05,
		"debug": true,
		"length": 5,
	})
	var inv = inventory_ui.get_inventory()
	print("Inventory/ies added")
	
	print("Set slots size to 63")
	inv.size = 63
	
	print("Changing color to green")
	var cells_manager:CellsManager = inventory_ui.get_cells_manager()
	cells_manager.set_cells_textures({
		"texture_normal":
			load("res://godot-libs/art/inventory_cells/green_cells/normal.png"),
		"texture_disabled": load("res://godot-libs/art/inventory_cells" + \
				"/green_cells/disabled.png"),
		"texture_focused": load("res://godot-libs/art/inventory_cells" + \
				"/green_cells/focused.png"),
		"texture_hover": load("res://godot-libs/art/inventory_cells/" + \
				"green_cells/highlight.png"),
		"texture_pressed": load("res://godot-libs/art/inventory_cells/" + \
				"green_cells/highlight.png"),
	})


##
func create_inventory_manager():
	print("--- Inventory manager test --- ")
	print("Instancing inventory manager scene")
	var inv_mng = InventoryManagerScene.instance()
	add_child(inv_mng)
	
	inv_mng.debug = true
	
	# Load inventory interfaces(scenes)
	var InventoryUI = preload("res://godot-libs/inventory_ui/" + \
			"inventory/inventory.tscn")
	var Hotbar = preload("res://godot-libs/inventory_ui/hotbar/hotbar.tscn")
	
	print("Adding 2 hotbars")
	var hotbar_scene1 = inv_mng.add_inventory_scene(Hotbar)
	var hotbar_scene2 = inv_mng.add_inventory_scene(Hotbar)
	print("Adding an inventory_ui ")
	var inventory_ui = inv_mng.add_inventory_scene(InventoryUI)
	inventory_ui.get_cells_manager().set_info({
		"cells_min_size": get_viewport().size.x * 0.05,
		"debug": true,
		"length": 5,
	})
	
	# Inventory scenes
	var hotbar1 = hotbar_scene1.get_inventory()
	var hotbar2 = hotbar_scene1.get_inventory()
	# The inventory script
	var inv = inventory_ui.get_inventory()
	print("Inventory/ies added")
	
	print("Debug: ", inv.debug)
	print("Size: ", inv.size)
	print("Items: ", inv.items)
	
	print("Set slots size to 63")
	inv.size = 63
