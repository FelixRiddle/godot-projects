extends KinematicBody2D

export (int) var speed = 200

# Private fields
var velocity = Vector2()
var input_handler = preload("res://godot-libs/inputs/" +
	"input_handler.gd").new({ "disable_wasd": false,
	"disable_arrows": false, "disable_joystick": false, })

# For more information 2D movement
# https://docs.godotengine.org/en/stable/tutorials/2d/2d_movement.html
func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)


func get_input():
	velocity = Vector2()
	if(input_handler.right()):
		velocity.x += 1
	if(input_handler.left()):
		velocity.x -= 1
	if(input_handler.up()):
		velocity.y -= 1
	if(input_handler.down()):
		velocity.y += 1
	
	# With normalize we prevent the player from having
	# extra speed while moving diagonally
	velocity = velocity.normalized() * speed
