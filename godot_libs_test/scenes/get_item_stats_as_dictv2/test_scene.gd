tool
extends Node

var Item = preload("res://godot-libs/inventory/items/item.gd")

func _init() -> void:
	test_1()
	test_2()
	return


func test_2():
	print(" --- Test 2: Instancing an inventory and creating items ---")
	# Inventory script
	var inv = Inventory.new()
	
	print("Adding five swords")
	inv.add_item_by_id(1, 5)
	print("Items name array: ")
	inv.print_items_name_array()
	
	#
	var first_item = inv.get_item_in_slot(1)
	print("First item: ", first_item)
	print("Its data in a dictionary: ",
			first_item.get_item_stats_as_dictv2())
	print("Its scene path: ", first_item.scene_path)


func test_1():
	print(" --- Test 1: Instancing an item directly and calling " + \
			"get_item_stats_as_dictv2 ---")
	var item = Item.new()
	
	print("Item stats in a dictionary: ", item.get_item_stats_as_dictv2())
